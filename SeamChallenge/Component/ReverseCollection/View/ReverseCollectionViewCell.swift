//  SeamChallenge.swift
//  SeamChallenge.swift
//
//  Created by ZainulAbideen on 30/05/19.
//  Copyright © 2019 ZainulAbideen. All rights reserved.

import UIKit

class ReverseCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var indexLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    func update(withData data: MySeamData) {
        
        nameLabel.text = data.name
        
        
        setImage(withURL: data.pic)
        
        indexLabel.text = "Index \(tag.description)"
        
        self.layoutIfNeeded()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        imageView.rounded()
        indexLabel.rounded()
    }
}

extension ReverseCollectionViewCell {
    
    
    private func setImage(withURL url: String?) {
        
        guard let url = url, url.count != 0 else {
            imageView.image = UIImage(named: "placeholde_icon")
            return
        }
        
        activityIndicator.startAnimating()
        imageView.setImage(from: url) {
            DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
            }
        }
    }
}

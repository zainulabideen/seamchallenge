//  SeamChallenge.swift
//  SeamChallenge.swift
//
//  Created by ZainulAbideen on 30/05/19.
//  Copyright © 2019 ZainulAbideen. All rights reserved.

import UIKit

class MySeamData: NSObject {
    
    var id: Int?
    
    var name = ""
    
    
    
    var pic: String?
    
    private override init() {
    }
    
    init(dictionary: [String: Any]) {
        
        if let id = dictionary["id"] as? Int {
            self.id = id
        }
        
        if let name = dictionary["name"] as? String {
            self.name = name
        }
        
        
        
        if let pic = dictionary["pic"] as? String {
            self.pic = pic
        }
    }
}


//  SeamChallenge.swift
//  SeamChallenge.swift
//
//  Created by ZainulAbideen on 30/05/19.
//  Copyright © 2019 ZainulAbideen. All rights reserved.

import Foundation

class ReverseCollectionModel {
    
    var mydata: [MySeamData] = []
    
    private var completion: (() -> Void)?
    
    init() {
        fetchData()
    }
    
    // Fetch Data from RealTime Database
    private func fetchData() {
        refreshMyData()
        FirebaseRealtimeDatabaseHelper.fetchMySeamData { (mydata, error) in
            if let myDataArray = mydata {
                for data in myDataArray {
                    self.mydata.append(MySeamData(dictionary: data))
                }
            }
            self.completion?()
        }
    }
    
    // Reset the Data while refreshing
    private func refreshMyData() {
        mydata = []
        completion?()
    }
}

extension ReverseCollectionModel {
    
    // Refresh the Data Realtime Database
    func refresh() {
        fetchData()
    }
    
    // Reload Data after successfully fetching
    //
    // - Parameter completion: Block
    func reloadData(_ completion: (() -> Void)?) {
        self.completion = completion
    }
}

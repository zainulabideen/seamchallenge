//  SeamChallenge.swift
//  SeamChallenge.swift
//
//  Created by ZainulAbideen on 31/05/19.
//  Copyright © 2019 ZainulAbideen. All rights reserved.

import Firebase

class FirebaseAuthHelper: NSObject {
    
    
    
    // Call this method from the application:didFinishLaunchingWithOptions: method, to initialize the FirebaseApp object
    static func configure() {
        // Use Firebase library to configure APIs
        FirebaseApp.configure()
    }
    
    /// Get Instance of Model
    static var shared: FirebaseAuthHelper = FirebaseAuthHelper()
    
    /// Listener Object
    private var handler: AuthStateDidChangeListenerHandle?
    
    var userId: String? {
        return Auth.auth().currentUser?.uid
    }
    
}

//MARK: - LISTENER
extension FirebaseAuthHelper {
    
    /// For each of your app's views that need information about the signed-in user, attach a listener to the FIRAuth object.
    /// This listener gets called whenever the user's sign-in state changes.
    /// Attach the listener in the view controller's viewWillAppear method:
    func addChangeListener() {
        
        handler = Auth.auth().addStateDidChangeListener { (auth, user) in
            // ...
            if let user = user {
                // The user's ID, unique to the Firebase project.
                // Do NOT use this value to authenticate with your backend server,
                // if you have one. Use getTokenWithCompletion:completion: instead.
                self.printUserDetails(user, withTag: "Add Listener")
            }
        }
    }
    
    // And detach the listener in the view controller's viewWillDisappear method:
    func removeChangeListener() {
        
        if (handler != nil) {
            Auth.auth().removeStateDidChangeListener(handler!)
        }
    }
}

//MARK: - Sign Up and Sign In
extension FirebaseAuthHelper {
    
    
    func createUser(email: String, password: String, completion: ((_ error: Error?) -> Void)? = nil) {
        
        Auth.auth().createUser(withEmail: email, password: password) { (authResult, error) in
            
            if let error = error {
                print("Error: \(error.localizedDescription)")
                completion?(error)
            }
            else if let user = authResult?.user {
                
                
                self.printUserDetails(user, withTag: "Create User")
                completion?(nil)
            }
        }
    }
    
    
    func signin(email: String, password: String, completion: ((_ error: Error?) -> Void)? = nil) {
        
        Auth.auth().signIn(withEmail: email, password: password) { (authResult, error) in
            // ...
            if let error = error {
                print("Error: \(error.localizedDescription)")
                completion?(error)
            }
            else if let user = authResult?.user {
                self.printUserDetails(user, withTag: "Sign In")
                completion?(nil)
            }
        }
    }
}

extension FirebaseAuthHelper {
    
    
    func profileChange(displayName: String, photoURL: String) {
        
        let changeRequest = Auth.auth().currentUser?.createProfileChangeRequest()
        
        changeRequest?.displayName = displayName
        changeRequest?.photoURL = URL(string: photoURL)
        
        changeRequest?.commitChanges { (error) in
            // ...
            
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
            else {
                print("Profile Change Request -> Committed Successfully.")
            }
        }
    }
    
    
    
    func updateEmail(email: String) {
        Auth.auth().currentUser?.updateEmail(to: email) { (error) in
            // ...
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
            else {
                print("Email Updated Successfully.")
            }
        }
    }
    
    
    func sendEmailVerification() {
        Auth.auth().currentUser?.sendEmailVerification { (error) in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
            else {
                print("Sent Email Verification Successfully.")
            }
        }
    }
    
    
    func updatePassword(password: String) {
        
        Auth.auth().currentUser?.updatePassword(to: password) { (error) in
            // ...
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
            else {
                print("Password updated Successfully.")
            }
        }
    }
    
    func sendPasswordReset(email: String) {
        
        Auth.auth().sendPasswordReset(withEmail: email) { (error) in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
            else {
                print("Sent password reset Successfully.")
            }
        }
    }
    
    func delete(_ completion: @escaping (_ success: Bool) -> Void) {
        
        let user = Auth.auth().currentUser
        
        user?.delete { error in
            if let error = error {
                print("Error Deleting account: \(error.localizedDescription)")
                completion(true)
            }
            else {
                print("Account Deleted Successfully.")
                completion(false)
            }
        }
    }
}

extension FirebaseAuthHelper {
    
    private func printUserDetails(_ user: User, withTag tag: String) {
        
        let uid = user.uid
        let name: String = user.displayName ?? ""
        let email: String = user.email ?? ""
        let photoURL = user.photoURL
        
        print("User uid: \(uid)")
        print("User name: \(name)")
        print("User email: \(email)")
        print("User photoURL: \(String(describing: photoURL?.description))")
    }
}

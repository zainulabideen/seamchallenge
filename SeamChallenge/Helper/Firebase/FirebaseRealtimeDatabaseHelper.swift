//  SeamChallenge.swift
//  SeamChallenge.swift
//
//  Created by ZainulAbideen on 31/05/19.
//  Copyright © 2019 ZainulAbideen. All rights reserved.

import Firebase

class FirebaseRealtimeDatabaseHelper: NSObject {
    
    private static let ref: DatabaseReference = Database.database().reference()
    static func fetchMySeamData(_ completion: @escaping (_ myData: [[String: Any]]?, _ error: Error?) -> Void) {
        
        ref.child("myData").observeSingleEvent(of: .value, with: { (snapshot) in
            if let data = snapshot.value as? [String: Any], let myData = data["data"] as? [[String: Any]] {
                print("\n\nValue: \(data)\n\n")
                completion(myData, nil)
            } else {
                completion(nil, nil)
            }
        }) { (error) in
            print("\n\nFetch data Error: \(error.localizedDescription)\n\n")
            completion(nil, error)
        }
    }
}

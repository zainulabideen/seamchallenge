//  SeamChallenge.swift
//  SeamChallenge.swift
//
//  Created by ZainulAbideen on 31/05/19.
//  Copyright © 2019 ZainulAbideen. All rights reserved.

import UIKit

extension UIImageView {
    
    
    func setImage(from url: String, completion: @escaping () -> Void) {
        
        guard let url = URL(string: url) else { return }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else {
                    completion()
                    return
            }
            
            DispatchQueue.main.async() {
                self.image = image
                completion()
            }
            }.resume()
    }
}

extension UIView {
    
    
    func rounded() {
        layer.borderWidth = 1
        layer.borderColor = UIColor.lightGray.cgColor
        layer.masksToBounds = true
    }
}

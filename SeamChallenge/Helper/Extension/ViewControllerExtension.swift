//  SeamChallenge.swift
//  SeamChallenge.swift
//
//  Created by ZainulAbideen on 31/05/19.
//  Copyright © 2019 ZainulAbideen. All rights reserved.

import UIKit

extension UIViewController {
    
    
    func showAlert(_ title: String = "", message: String) {
        
        // create the alert
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        // add an action (button)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
            // do something like...
        }))
        
        // show the alert
        self.present(alertController, animated: true, completion: nil)
    }
    
    /// Show Navigation
    func showNavigation() {
        self.navigationController?.setNavigationBarHidden(false, animated: false);
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    /// Hide Navigation
    func hideNavigation() {
        self.navigationController?.setNavigationBarHidden(true, animated: false);
        self.navigationController?.navigationBar.isTranslucent = true
    }
}
